local DIRECTION_NORTH = 1
local DIRECTION_EAST = 2
local DIRECTION_SOUTH = 3
local DIRECTION_WEST = 4

local deltaXs = {}
deltaXs[DIRECTION_NORTH]=0
deltaXs[DIRECTION_EAST]=1
deltaXs[DIRECTION_SOUTH]=0
deltaXs[DIRECTION_WEST]=-1

local deltaYs = {}
deltaYs[DIRECTION_NORTH]=1
deltaYs[DIRECTION_EAST]=0
deltaYs[DIRECTION_SOUTH]=-1
deltaYs[DIRECTION_WEST]=0

local lib = {}

function lib.DIRECTION_NORTH() return DIRECTION_NORTH end
function lib.DIRECTION_EAST() return DIRECTION_EAST end
function lib.DIRECTION_SOUTH() return DIRECTION_SOUTH end
function lib.DIRECTION_WEST() return DIRECTION_WEST end

function lib.getNextXY(direction, x, y)
	local nextX = x + deltaXs[direction]
	local nextY = y + deltaYs[direction]
	return nextX, nextY
end

return lib