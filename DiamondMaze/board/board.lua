local gameConstants = require("game.constants")
local boardColumn = require("board.column")
local gameTerrains = require("game.terrains")
local gameAvatar = require("game.avatar")
local gameItems = require("game.items")
local mazeMaze = require("maze.maze")
local mazeGenerator = require("maze.generator")
local mazeDirections = require("maze.directions")
local boardDirections = require("board.directions")
local gameDoorColors = require("game.doorcolors")
local utilitiesMisc = require("utilities.misc")

local lib = {}

local function doPlacements(board,count,columns,rows,cellChecker,placer)
	while count > 0 do
		local column = math.random(1,columns)
		local row=math.random(1,rows)
		local cell = board:getCell(column, row)
		if cellChecker(cell) then
			placer(cell)
			count = count - 1
		end
	end
end

function lib.create()
	local instance = {}
	instance.columns = {}
	for column=1,gameConstants.BOARD_COLUMNS() do
		instance.columns[column]=boardColumn.create(instance,column,gameConstants.BOARD_ROWS())
	end
	instance.avatar = gameAvatar.create()
	instance.bolts = {}
	instance.enemies = {}
	instance.enemySpawner = {}
	function instance:getColumn(column)
		return self.columns[column]
	end
	function instance:getCell(column,row)
		local c = self:getColumn(column)
		if c~=nil then
			return c:getCell(row)
		end
	end
	function instance:getAvatar()
		return self.avatar
	end
	function instance:placeAvatarInstance()
		local avatar = self:getAvatar()
		self:getCell(avatar:getColumn(),avatar:getRow()):placeCreatureInstance(avatar:getCreatureInstance())
	end
	function instance:removeAvatarInstance()
		local avatar = self:getAvatar()
		local avatarInstance = self:getCell(avatar:getColumn(),avatar:getRow()):getCreatureInstance()
		self:getCell(avatar:getColumn(),avatar:getRow()):placeCreatureInstance(nil)
	end
	function instance:removeDoor(column,row)
		local mazeColumn, mazeRow = utilitiesMisc.boardDiv(column, row, gameConstants.ROOM_COLUMNS(), gameConstants.ROOM_ROWS())
		for roomColumn= 1, gameConstants.ROOM_COLUMNS() do
			for roomRow = 1, gameConstants.ROOM_ROWS() do
				local column = (mazeColumn-1) * gameConstants.ROOM_COLUMNS() + roomColumn
				local row = (mazeRow-1) * gameConstants.ROOM_ROWS() + roomRow
				local cell = self:getCell(column, row)
				local terrain = cell:getTerrain()
				if gameTerrains.isLock(terrain) or gameTerrains.isDoor(terrain) then
					cell:setTerrain(gameTerrains.TERRAIN_FLOOR())
				end
			end
		end
	end
	function instance:placeBolt(bolt)
		local cell = self:getCell(bolt:getColumn(),bolt:getRow())
		if cell~=nil and cell:getItem()==nil then
			if cell:getCreatureInstance()~=nil then
				local creatureInstance = cell:getCreatureInstance()
				--TODO: damage creature
			else
				local terrain = cell:getTerrain()
				if gameTerrains.canHasBolt(terrain) then
					cell:setItem(bolt:getItem())
					return true
				end
			end
		end
		return false
	end
	function instance:addBolt(bolt)
		if self:placeBolt(bolt) then
			table.insert(self.bolts, bolt)
		end
	end
	function instance:updateBolts(deltaTime)
		for index = #self.bolts, 1, -1 do
			local bolt = self.bolts[index]
			local cell = self:getCell(bolt:getColumn(),bolt:getRow())
			cell:setItem(nil)
			bolt:advanceTimer(deltaTime)
			if not self:placeBolt(bolt) then
				table.remove(self.bolts, index)
			end
		end
	end
	

	for column=1,gameConstants.BOARD_COLUMNS() do
		for row=1,gameConstants.BOARD_ROWS() do
			if column==1 or row==1 or column == gameConstants.BOARD_COLUMNS() or row == gameConstants.BOARD_ROWS() then
				instance:getCell(column, row):setTerrain(gameTerrains.TERRAIN_WALL())
			else
				local roomPositionX = column % gameConstants.ROOM_COLUMNS()
				local roomPositionY = row % gameConstants.ROOM_ROWS()
				if roomPositionX < 2 and roomPositionY<2 then
					instance:getCell(column, row):setTerrain(gameTerrains.TERRAIN_WALL())
				else					
					instance:getCell(column, row):setTerrain(gameTerrains.TERRAIN_FLOOR())
				end
			end
		end
	end

	local maze = mazeMaze.new(gameConstants.MAZE_COLUMNS(), gameConstants.MAZE_ROWS(), mazeDirections)
	mazeGenerator.generate(maze,mazeDirections)
	local doorColorCounts = {}
	for doorColor = 1, gameConstants.DOOR_COLORS() do
		doorColorCounts[doorColor]=0
	end
	for mazeColumn = 1, gameConstants.MAZE_COLUMNS() do
		for mazeRow = 1, gameConstants.MAZE_ROWS() do
			local mazeCell = maze:getCell(mazeColumn,mazeRow)
			local offsetX = (mazeColumn-1) * gameConstants.ROOM_COLUMNS()
			local offsetY = (mazeRow-1) * gameConstants.ROOM_ROWS()
			local wallCount = 0
			local exits = {}
			--north wall
			if mazeCell:hasWall(boardDirections.DIRECTION_NORTH()) then
				wallCount = wallCount + 1
				for roomColumn=1, gameConstants.ROOM_COLUMNS() do
					instance:getCell(offsetX+roomColumn, offsetY+gameConstants.ROOM_ROWS()):setTerrain(gameTerrains.TERRAIN_WALL())
				end
			else
				table.insert(exits, boardDirections.DIRECTION_NORTH())
			end
			--east wall
			if mazeCell:hasWall(boardDirections.DIRECTION_EAST()) then
				wallCount = wallCount + 1
				for roomRow=1, gameConstants.ROOM_ROWS() do
					instance:getCell(offsetX+gameConstants.ROOM_COLUMNS(), offsetY+roomRow):setTerrain(gameTerrains.TERRAIN_WALL())
				end
			else
				table.insert(exits, boardDirections.DIRECTION_EAST())
			end
			--south wall
			if mazeCell:hasWall(boardDirections.DIRECTION_SOUTH()) then
				wallCount = wallCount + 1
				for roomColumn=1, gameConstants.ROOM_COLUMNS() do
					instance:getCell(offsetX+roomColumn, offsetY+1):setTerrain(gameTerrains.TERRAIN_WALL())
				end
			else
				table.insert(exits, boardDirections.DIRECTION_SOUTH())
			end
			--west wall
			if mazeCell:hasWall(boardDirections.DIRECTION_WEST()) then
				wallCount = wallCount + 1
				for roomRow=1, gameConstants.ROOM_ROWS() do
					instance:getCell(offsetX+1, offsetY+roomRow):setTerrain(gameTerrains.TERRAIN_WALL())
				end
			else
				table.insert(exits, boardDirections.DIRECTION_WEST())
			end
			if wallCount==3 then
				--dead end!
				for roomColumn=1,gameConstants.ROOM_COLUMNS() do
					for roomRow=1,gameConstants.ROOM_ROWS() do
						local cell = instance:getCell(offsetX+roomColumn, offsetY+roomRow)
						if cell:getTerrain()==gameTerrains.TERRAIN_FLOOR() then
							cell:setTerrain(gameTerrains.TERRAIN_DEADEND())
						end
					end
				end
				local exitDirection = exits[1]
				--determine color of door
				local doorColor = math.random(1,gameConstants.DOOR_COLORS())
				doorColorCounts[doorColor] = doorColorCounts[doorColor] + 1
				local doorTerrain = gameDoorColors.getDoorTerrain(doorColor)
				local lockTerrain = gameDoorColors.getLockTerrain(doorColor)
				--determine rectangle for door
				local x1=0
				local x2=0
				local y1=0
				local y2=0
				if exitDirection==boardDirections.DIRECTION_NORTH() then
					x1 = offsetX + 2
					x2 = offsetX + gameConstants.ROOM_COLUMNS()-1
					y1 = offsetY + gameConstants.ROOM_ROWS()
					y2 = offsetY + gameConstants.ROOM_ROWS()
				elseif exitDirection==boardDirections.DIRECTION_EAST() then
					x1 = offsetX + gameConstants.ROOM_COLUMNS()
					x2 = offsetX + gameConstants.ROOM_COLUMNS()
					y1 = offsetY + 2
					y2 = offsetY + gameConstants.ROOM_ROWS()-1
				elseif exitDirection==boardDirections.DIRECTION_SOUTH() then
					x1 = offsetX + 2
					x2 = offsetX + gameConstants.ROOM_COLUMNS()-1
					y1 = offsetY + 1
					y2 = offsetY + 1
				elseif exitDirection==boardDirections.DIRECTION_WEST() then
					x1 = offsetX + 1
					x2 = offsetX + 1
					y1 = offsetY + 2
					y2 = offsetY + gameConstants.ROOM_ROWS()-1
				end
				--determine lock location for door
				local lx = math.random(x1,x2)
				local ly = math.random(y1,y2)
				for x=x1,x2 do
					for y = y1, y2 do
						if x==lx and y==ly then
							instance:getCell(x, y):setTerrain(lockTerrain)
						else
							instance:getCell(x, y):setTerrain(doorTerrain)
						end
					end
				end
			end
		end
	end

	--place goodie huts
	local goodieHutCount = gameConstants.GOODIEHUT_COUNT()
	while goodieHutCount>0 do
		local column = math.random(1,gameConstants.BOARD_COLUMNS())
		local row=math.random(1,gameConstants.BOARD_ROWS())
		local cell = instance:getCell(column, row)
		if cell:getTerrain()==gameTerrains.TERRAIN_FLOOR() and cell:getItem()==nil and cell:getCreatureInstance()==nil then
			cell:setTerrain(gameTerrains.TERRAIN_GOODIEHUT())
			goodieHutCount = goodieHutCount - 1
		end
	end

	--place keys
	for doorColor = 1, gameConstants.DOOR_COLORS() do
		while doorColorCounts[doorColor]>0 do
			local column = math.random(1,gameConstants.BOARD_COLUMNS())
			local row=math.random(1,gameConstants.BOARD_ROWS())
			local cell = instance:getCell(column, row)
			if cell:getTerrain()==gameTerrains.TERRAIN_FLOOR() and cell:getItem()==nil and cell:getCreatureInstance()==nil then
				cell:setItem(gameDoorColors.getKeyItem(doorColor))
				doorColorCounts[doorColor] = doorColorCounts[doorColor] - 1
			end
		end
	end

	--place diamonds
	--non dead end diamonds
	doPlacements(instance, gameConstants.DIAMONDS_NONDEADEND(), gameConstants.BOARD_COLUMNS(), gameConstants.BOARD_ROWS(), function(cell) return cell:getTerrain()==gameTerrains.TERRAIN_FLOOR() and cell:getItem()==nil and cell:getCreatureInstance()==nil end, function(cell) cell:setItem(gameItems.ITEM_DIAMOND()) end)
	--dead end diamonds
	doPlacements(instance, gameConstants.DIAMONDS_DEADEND(), gameConstants.BOARD_COLUMNS(), gameConstants.BOARD_ROWS(), function(cell) return cell:getTerrain()==gameTerrains.TERRAIN_DEADEND() and cell:getItem()==nil and cell:getCreatureInstance()==nil end, function(cell) cell:setItem(gameItems.ITEM_DIAMOND()) end)
	--place enemy spawners
	
	--place avatar	
	local placed = false
	while not placed do
		local column = math.random(1,gameConstants.BOARD_COLUMNS())
		local row=math.random(1,gameConstants.BOARD_ROWS())
		local cell = instance:getCell(column, row)
		if cell:getTerrain()==gameTerrains.TERRAIN_FLOOR() and cell:getItem()==nil and cell:getCreatureInstance()==nil then
			instance:getAvatar():setColumn(column)
			instance:getAvatar():setRow(row)
			placed = true
		end
	end
	instance:placeAvatarInstance()
	
	return instance
end

return lib