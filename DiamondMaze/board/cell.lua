local gameTerrains = require("game.terrains")

local lib = {}

function lib.create(boardColumn,row)
	local instance = {}
	instance.boardColumn = boardColumn
	instance.row = row
	instance.terrain = gameTerrains.TERRAIN_FLOOR()
	function instance:getBoardColumn()
		return self.boardColumn
	end
	function instance:getRow()
		return self.row
	end
	function instance:getColumn()
		return self:getBoardColumn():getColumn()
	end
	function instance:getBoard()
		return self:getBoardColumn():getBoard()
	end
	function instance:getTerrain()
		return self.terrain
	end
	function instance:setTerrain(newTerrain)
		self.terrain = newTerrain
	end
	function instance:getCreatureInstance()
		return self.creatureInstance
	end
	function instance:placeCreatureInstance(newCreatureInstance)
		self.creatureInstance = newCreatureInstance
	end
	function instance:getItem()
		return self.item
	end
	function instance:setItem(item)
		self.item = item
	end
	return instance
end

return lib