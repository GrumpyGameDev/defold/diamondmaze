local boardCell = require("board.cell")

local lib = {}

function lib.create(board,column,rows)
	local instance = {}
	instance.board = board
	instance.column = column
	instance.cells = {}
	for row=1,rows do
		instance.cells[row]=boardCell.create(instance,row)
	end
	function instance:getBoard() return self.board end
	function instance:getColumn() return self.column end
	function instance:getCell(row) 
		return self.cells[row]
	end
	return instance
end

return lib