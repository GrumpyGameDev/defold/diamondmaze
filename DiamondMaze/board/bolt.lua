local boardDirections = require("board.directions")
local gameItems = require("game.items")

local lib = {}

local itemTable = {}
itemTable[boardDirections.DIRECTION_NORTH()]=gameItems.ITEM_AVATARBOLTNORTH()
itemTable[boardDirections.DIRECTION_EAST()]=gameItems.ITEM_AVATARBOLTEAST()
itemTable[boardDirections.DIRECTION_SOUTH()]=gameItems.ITEM_AVATARBOLTSOUTH()
itemTable[boardDirections.DIRECTION_WEST()]=gameItems.ITEM_AVATARBOLTWEST()

function lib.create(column,row,facing,moveInterval)
	local instance = {}
	instance.column = column
	instance.row = row
	instance.facing = facing
	instance.moveInterval = moveInterval
	instance.moveTimer = 0
	function instance:getColumn()
		return self.column
	end
	function instance:getRow()
		return self.row
	end
	function instance:getFacing()
		return self.facing
	end
	function instance:getItem()
		return itemTable[self:getFacing()]
	end
	function instance:advanceTimer(deltaTime)
		self.moveTimer = self.moveTimer + deltaTime
		if self.moveTimer >= self.moveInterval then
			self.moveTimer = self.moveTimer - self.moveInterval
			local nextColumn, nextRow = boardDirections.getNextXY(self:getFacing(), self:getColumn(), self:getRow())
			self.column = nextColumn
			self.row = nextRow
		end
	end
	return instance
end

return lib