local lib = {}

function lib.boardDiv(column,row,columns,rows)
	local c = math.floor((column-1) / columns) + 1
	local r = math.floor((row-1) / rows) + 1
	return c,r
end

function lib.boardMod(column,row,columns,rows)
	local c = math.floor((column-1) % columns) + 1
	local r = math.floor((row-1) % rows) + 1
	return c,r
end

return lib