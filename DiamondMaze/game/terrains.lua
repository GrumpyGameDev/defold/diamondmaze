local gameBoardTiles = require("game.boardtiles")
local gameItems = require("game.items")
local TERRAIN_FLOOR = "floor"
local TERRAIN_WALL = "wall"
local TERRAIN_DEADEND = "dead-end"
local TERRAIN_REDDOOR = "red-door"
local TERRAIN_GREENDOOR = "green-door"
local TERRAIN_BLUEDOOR = "blue-door"
local TERRAIN_YELLOWDOOR = "yellow-door"
local TERRAIN_REDLOCK = "red-lock"
local TERRAIN_GREENLOCK = "green-lock"
local TERRAIN_BLUELOCK = "blue-lock"
local TERRAIN_YELLOWLOCK = "yellow-lock"
local TERRAIN_GOODIEHUT = "goodie-hut"

local function makeDescriptor(tile,isOpen,isLock,keyItem,isDoor,canHasBolt)
	local instance = {}
	instance.tile = tile
	instance.isOpen = isOpen
	instance.isLock = isLock
	instance.keyItem = keyItem
	instance.isDoor = isDoor
	instance.canHasBolt = canHasBolt
	return instance
end

local descriptors = {}
descriptors[TERRAIN_FLOOR] = makeDescriptor(gameBoardTiles.TILE_NONE(), true,  false, nil, false, true)
descriptors[TERRAIN_WALL] = makeDescriptor(gameBoardTiles.TILE_WALL(), false,  false, nil, false, false)
descriptors[TERRAIN_DEADEND] = makeDescriptor(gameBoardTiles.TILE_NONE(), true, false, nil, false, true)
descriptors[TERRAIN_REDDOOR] = makeDescriptor(gameBoardTiles.TILE_REDDOOR(), false,  false, nil, true, false)
descriptors[TERRAIN_GREENDOOR] = makeDescriptor(gameBoardTiles.TILE_GREENDOOR(), false,  false, nil, true, false)
descriptors[TERRAIN_BLUEDOOR] = makeDescriptor(gameBoardTiles.TILE_BLUEDOOR(), false,  false, nil, true, false)
descriptors[TERRAIN_YELLOWDOOR] = makeDescriptor(gameBoardTiles.TILE_YELLOWDOOR(), false,  false, nil, true, false)
descriptors[TERRAIN_REDLOCK] = makeDescriptor(gameBoardTiles.TILE_REDLOCK(), false, true, gameItems.ITEM_REDKEY(), false, false)
descriptors[TERRAIN_GREENLOCK] = makeDescriptor(gameBoardTiles.TILE_GREENLOCK(), false, true, gameItems.ITEM_GREENKEY(), false, false)
descriptors[TERRAIN_BLUELOCK] = makeDescriptor(gameBoardTiles.TILE_BLUELOCK(), false, true, gameItems.ITEM_BLUEKEY(), false, false)
descriptors[TERRAIN_YELLOWLOCK] = makeDescriptor(gameBoardTiles.TILE_YELLOWLOCK(), false, true, gameItems.ITEM_YELLOWKEY(), false, false)
descriptors[TERRAIN_GOODIEHUT] = makeDescriptor(gameBoardTiles.TILE_GOODIEHUT(), false, false, nil, false, false)

local lib = {}

function lib.TERRAIN_FLOOR() return TERRAIN_FLOOR end
function lib.TERRAIN_WALL() return TERRAIN_WALL end
function lib.TERRAIN_DEADEND() return TERRAIN_DEADEND end
function lib.TERRAIN_REDDOOR() return TERRAIN_REDDOOR end
function lib.TERRAIN_GREENDOOR() return TERRAIN_GREENDOOR end
function lib.TERRAIN_BLUEDOOR() return TERRAIN_BLUEDOOR end
function lib.TERRAIN_YELLOWDOOR() return TERRAIN_YELLOWDOOR end
function lib.TERRAIN_REDLOCK() return TERRAIN_REDLOCK end
function lib.TERRAIN_GREENLOCK() return TERRAIN_GREENLOCK end
function lib.TERRAIN_BLUELOCK() return TERRAIN_BLUELOCK end
function lib.TERRAIN_YELLOWLOCK() return TERRAIN_YELLOWLOCK end
function lib.TERRAIN_GOODIEHUT() return TERRAIN_GOODIEHUT end

function lib.getTile(terrain)
	return descriptors[terrain].tile
end

function lib.isOpen(terrain)
	return descriptors[terrain].isOpen
end

function lib.isDoor(terrain)
	return descriptors[terrain].isDoor
end

function lib.isLock(terrain)
	return descriptors[terrain].isLock
end

function lib.getKeyItem(terrain)
	return descriptors[terrain].keyItem
end

function lib.canHasBolt(terrain)
	return descriptors[terrain].canHasBolt
end

return lib