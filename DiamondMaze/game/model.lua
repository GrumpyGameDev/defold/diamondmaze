local boardBoard = require("board.board")

local board = nil

local module = {}

function module.reset()
	board = boardBoard.create()
end

function module.getBoard()
	return board
end

return module