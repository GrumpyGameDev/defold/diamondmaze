local gameCommonCreature = require("game.commoncreature")
local gameConstants = require("game.constants")
local lib = {}

function lib.create(column,row)
	local instance = gameCommonCreature.create(column, row, gameConstants.ENEMY_HEALTH(), gameConstants.ENEMY_MOVECOOLDOWN(), gameConstants.ENEMY_ATTACKCOOLDOWN())
	return instance
end

return lib