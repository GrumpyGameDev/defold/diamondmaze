local lib = {}

function lib.create(column,row,health,moveCoolDownInterval,attackCoolDownInterval)
	local instance = {}
	instance.column = column
	instance.row = row
	instance.health = health
	instance.attackCoolDown = 0
	instance.attackCoolDownInterval = attackCoolDownInterval
	
	function instance:getColumn() return self.column end
	function instance:getRow() return self.row end
	function instance:setColumn(column)
		self.column = column
	end
	function instance:setRow(row)
		self.row = row
	end
	function instance:isAttackCoolingDown()
		return self.attackCoolDown>0
	end
	function instance:updateAttackCoolDown(deltaTime)
		if deltaTime > instance.attackCoolDown then
			self.attackCoolDown = 0
		else	
			self.attackCoolDown = self.attackCoolDown - deltaTime
		end
	end
	function instance:startAttackCoolDown()
		self.attackCoolDown = self.attackCoolDownInterval
	end
	if moveCoolDownInterval ~=nil then
		instance.moveCoolDown = 0
		instance.moveCoolDownInterval = moveCoolDownInterval
		function instance:isMoveCoolingDown()
			return self.moveCoolDown>0
		end
		function instance:updateMoveCoolDown(deltaTime)
			if deltaTime > self.moveCoolDown then
				self.moveCoolDown = 0
			else
				self.moveCoolDown = self.moveCoolDown - deltaTime
			end
		end
		function instance:startMoveCoolDown()
			self.moveCoolDown = self.moveCoolDownInterval
		end
	end
			
	return instance
end

return lib