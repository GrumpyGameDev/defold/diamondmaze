local gameMessageIds = require("game.messageids")
local gameMessages = require("game.messages")
local utilitiesUrls = require("utilities.urls")
local boardBoard = require("board.board")
local gameConstants = require("game.constants")
local gameModel = require("game.model")
local gameStates = require("game.states")

local currentState = gameStates.STATE_NONE()
local states = {}
states[gameStates.STATE_NEWGAME()]=require("game.states.newgame")
states[gameStates.STATE_INPLAY()]=require("game.states.inplay")

local module = {}

function module.getCurrentState() return currentState end

function module.setCurrentState(newState)
	local finishingState = states[currentState]
	if finishingState~=nil and finishingState.finish~=nil then
		finishingState.finish()
	end
	currentState = newState
	local startingState = states[currentState]
	if startingState~=nil and startingState.start~=nil then
		startingState.start()
	end
end

function module.handleCommand(command)
	local handlingState = states[currentState]
	if handlingState~=nil and handlingState.handleCommand~=nil then
		handlingState.handleCommand(command)
	end
end

function module.handleUpdate(deltaTime)
	local handlingState = states[currentState]
	if handlingState~=nil and handlingState.handleUpdate~=nil then
		handlingState.handleUpdate(deltaTime)
	end
end

return module