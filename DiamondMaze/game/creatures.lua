local gameBoardTiles = require("game.boardtiles")
local CREATURE_AVATAR = "avatar"
local CREATURE_ENEMY = "enemy"
local CREATURE_ENEMYSPAWNER = "enemy-spawner"

local tiles = {}
tiles[CREATURE_AVATAR]=gameBoardTiles.TILE_AVATAR()
tiles[CREATURE_ENEMY]=gameBoardTiles.TILE_ENEMY()
tiles[CREATURE_ENEMYSPAWNER]=gameBoardTiles.TILE_ENEMYSPAWNER()

local module = {}

function module.CREATURE_AVATAR() return CREATURE_AVATAR end
function module.CREATURE_ENEMY() return CREATURE_ENEMY end
function module.CREATURE_ENEMYSPAWNER() return CREATURE_ENEMYSPAWNER end

function module.getTile(creature)
	return tiles[creature]
end

return module