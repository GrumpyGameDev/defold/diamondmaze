local module = {}

function module.makeChangeState(newState)
	local instance = {}
	instance.newState = newState
	return instance
end

return module