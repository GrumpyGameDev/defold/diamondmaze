local gameBoardTiles = require("game.boardtiles")
local ITEM_REDKEY = "red-key"
local ITEM_GREENKEY = "green-key"
local ITEM_BLUEKEY = "blue-key"
local ITEM_YELLOWKEY = "yellow-key"
local ITEM_DIAMOND = "diamond"
local ITEM_AVATARBOLTNORTH = "avatar-bolt-north"
local ITEM_AVATARBOLTEAST = "avatar-bolt-east"
local ITEM_AVATARBOLTSOUTH = "avatar-bolt-south"
local ITEM_AVATARBOLTWEST = "avatar-bolt-west"

local tiles = {}
tiles[ITEM_REDKEY] = gameBoardTiles.TILE_REDKEY()
tiles[ITEM_GREENKEY] = gameBoardTiles.TILE_GREENKEY()
tiles[ITEM_BLUEKEY] = gameBoardTiles.TILE_BLUEKEY()
tiles[ITEM_YELLOWKEY] = gameBoardTiles.TILE_YELLOWKEY()
tiles[ITEM_DIAMOND] = gameBoardTiles.TILE_DIAMOND()
tiles[ITEM_AVATARBOLTNORTH] = gameBoardTiles.TILE_AVATARBOLTNORTH()
tiles[ITEM_AVATARBOLTEAST] = gameBoardTiles.TILE_AVATARBOLTEAST()
tiles[ITEM_AVATARBOLTSOUTH] = gameBoardTiles.TILE_AVATARBOLTSOUTH()
tiles[ITEM_AVATARBOLTWEST] = gameBoardTiles.TILE_AVATARBOLTWEST()

local lib = {}

function lib.ITEM_REDKEY() return ITEM_REDKEY end
function lib.ITEM_GREENKEY() return ITEM_GREENKEY end
function lib.ITEM_BLUEKEY() return ITEM_BLUEKEY end
function lib.ITEM_YELLOWKEY() return ITEM_YELLOWKEY end
function lib.ITEM_DIAMOND() return ITEM_DIAMOND end
function lib.ITEM_AVATARBOLTNORTH() return ITEM_AVATARBOLTNORTH end
function lib.ITEM_AVATARBOLTEAST() return ITEM_AVATARBOLTEAST end
function lib.ITEM_AVATARBOLTSOUTH() return ITEM_AVATARBOLTSOUTH end
function lib.ITEM_AVATARBOLTWEST() return ITEM_AVATARBOLTWEST end

function lib.getTile(item)
	return tiles[item]
end

return lib