local gameCreatures = require("game.creatures")
local gameCreatureInstance = require("game.creatureinstance")
local boardDirections = require("board.directions")
local gameConstants = require("game.constants")
local gameCommonCreature = require("game.commoncreature")
local lib = {}

function lib.create()
	local instance = gameCommonCreature.create(1,1,gameConstants.HEALTH_MAXIMUM(),gameConstants.AVATAR_MOVECOOLDOWN(),gameConstants.AVATAR_ATTACKDOOLDOWN())
	
	instance.facing = boardDirections.DIRECTION_NORTH()
	instance.creatureInstance = gameCreatureInstance.create(gameCreatures.CREATURE_AVATAR())
	instance.inventory = {}
	instance.diamonds = 0
	instance.depositedDiamonds = 0
	instance.mana = gameConstants.MANA_MAXIMUM()
	instance.manaRegenerationCoolDown = gameConstants.AVATAR_MANAREGENERATIONCOOLDOWN()

	function instance:getFacing() return self.facing end
	function instance:getCreatureInstance() return self.creatureInstance end
	function instance:isFiring() return self.firing end
	function instance:setFacing(facing)
		self.facing = facing
	end
	function instance:getInventory()
		return self.inventory
	end
	function instance:hasItem(item)
		for i,v in ipairs(self.inventory) do
			if v == item then
				return true
			end
		end
		return false
	end
	function instance:isInventoryFull()
		return (#self.inventory) >= gameConstants.INVENTORY_SLOTS()
	end
	function instance:addItem(item)
		if self:isInventoryFull() then
			return false
		else
			table.insert(self.inventory, item)
			return true
		end
	end
	function instance:removeItem(item)
		for i, v in ipairs(self.inventory) do
			if v == item then
				table.remove(self.inventory,i)
				return true
			end
		end
		return false
	end
	function instance:getDiamonds()
		return self.diamonds
	end
	function instance:isAtDiamondCapacity()
		return self.diamonds >= gameConstants.DIAMOND_CAPACITY()
	end
	function instance:addDiamond()
		self.diamonds = self.diamonds + 1
	end
	function instance:depositDiamonds()
		self.depositedDiamonds = self.depositedDiamonds + self.diamonds
		self.diamonds = 0
	end
	function instance:getHealth()
		return self.health
	end
	function instance:setHealth(health)
		self.health = health
	end
	function instance:getMana()
		return self.mana
	end
	function instance:setMana(mana)
		if mana<0 then
			mana = 0
		elseif mana>gameConstants.MANA_MAXIMUM() then
			mana = gameConstants.MANA_MAXIMUM()
		end
		self.mana = mana
	end
	function instance:updateManaRegenerationCoolDown(deltaTime)
		self.manaRegenerationCoolDown = self.manaRegenerationCoolDown - deltaTime
		while self.manaRegenerationCoolDown < 0 do
			self.manaRegenerationCoolDown = self.manaRegenerationCoolDown + gameConstants.AVATAR_MANAREGENERATIONCOOLDOWN()
			self:setMana(self:getMana()+gameConstants.AVATAR_MANAREGENERATIONRATE())
		end
	end
	function instance:updateCoolDowns(deltaTime)
		self:updateAttackCoolDown(deltaTime)
		self:updateMoveCoolDown(deltaTime)
		self:updateManaRegenerationCoolDown(deltaTime)
	end
	function instance:canAttack()
		return self:getMana() >= gameConstants.AVATAR_ATTACKMANA() and not self:isAttackCoolingDown()
	end
	function instance:doAttack()
		if self:canAttack() then
			self:setMana(self:getMana() - gameConstants.AVATAR_ATTACKMANA())
			self:startAttackCoolDown()
			self:startMoveCoolDown()
			return true
		else
			return false
		end
	end
	return instance
end

return lib