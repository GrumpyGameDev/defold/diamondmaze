local lib = {}

function lib.create(creature)
	local instance = {}
	instance.creature = creature
	function instance:getCreature() return self.creature end
	return instance
end

return lib