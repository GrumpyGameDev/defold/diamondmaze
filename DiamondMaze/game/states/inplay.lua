local boardDirections = require("board.directions")
local gameCommands = require("game.commands")
local gameModel = require("game.model")
local gameConstants = require("game.constants")
local gameTerrains = require("game.terrains")
local gameItems = require("game.items")
local boardBolt = require("board.bolt")
local gameSounds = require("game.sounds")

local directionTable = {}
directionTable[gameCommands.COMMAND_UP()]=boardDirections.DIRECTION_NORTH()
directionTable[gameCommands.COMMAND_RIGHT()]=boardDirections.DIRECTION_EAST()
directionTable[gameCommands.COMMAND_DOWN()]=boardDirections.DIRECTION_SOUTH()
directionTable[gameCommands.COMMAND_LEFT()]=boardDirections.DIRECTION_WEST()

local module = {}

function module.start()
end

function module.finish()
end

function module.handleCommand(command)
	local board = gameModel.getBoard()
	local avatar = board:getAvatar()

	local direction = directionTable[command]
	if direction~=nil then
		if not avatar:isMoveCoolingDown() then
			avatar:startMoveCoolDown()
			if direction==avatar:getFacing() then
				board:removeAvatarInstance()
				local nextColumn, nextRow = boardDirections.getNextXY(direction, avatar:getColumn(), avatar:getRow())
				if nextColumn<1 or nextRow<1 or nextColumn > gameConstants.BOARD_COLUMNS() or nextRow > gameConstants.BOARD_ROWS() then
					nextColumn = avatar:getColumn()
					nextRow = avatar:getRow()
				else
					local cell = board:getCell(nextColumn, nextRow)
					local terrain = cell:getTerrain()
					if not gameTerrains.isOpen(terrain) then
						if gameTerrains.isLock(terrain) then
							local keyItem = gameTerrains.getKeyItem(terrain)
							assert(keyItem~=nil)
							if avatar:hasItem(keyItem) then
								sound.play(gameSounds.SOUND_KACLICK())
								avatar:removeItem(keyItem)
								board:removeDoor(nextColumn,nextRow)
							else
								nextColumn = avatar:getColumn()
								nextRow = avatar:getRow()
							end
						elseif terrain == gameTerrains.TERRAIN_GOODIEHUT() then
							avatar:depositDiamonds()
							--TODO: sound effect (if diamonds actually deposited)
							nextColumn = avatar:getColumn()
							nextRow = avatar:getRow()
						else
							nextColumn = avatar:getColumn()
							nextRow = avatar:getRow()
						end
					end
					local item = cell:getItem()
					if item~=nil then
						if item==gameItems.ITEM_DIAMOND() then
							if avatar:isAtDiamondCapacity() then
								nextColumn = avatar:getColumn()
								nextRow = avatar:getRow()
							else
								avatar:addDiamond()
								sound.play(gameSounds.SOUND_TING())
								cell:setItem(nil)
							end
						elseif avatar:isInventoryFull() then
							nextColumn = avatar:getColumn()
							nextRow = avatar:getRow()
						else
							avatar:addItem(item)
							sound.play(gameSounds.SOUND_KEYPICKUP())
							cell:setItem(nil)
						end
					end
				end
				avatar:setColumn(nextColumn)
				avatar:setRow(nextRow)
				board:placeAvatarInstance()
			else
				avatar:setFacing(direction)
			end
		end
	elseif command==gameCommands.COMMAND_GREEN() then
		if avatar:canAttack() then
			avatar:doAttack()
			local nextColumn, nextRow = boardDirections.getNextXY(avatar:getFacing(), avatar:getColumn(), avatar:getRow())
			local bolt = boardBolt.create(nextColumn, nextRow, avatar:getFacing(),gameConstants.BOLT_MOVEINTERVAL())
			board:addBolt(bolt)
			sound.play(gameSounds.SOUND_PTOO())
		end
	end
end

function module.handleUpdate(deltaTime)
	local board = gameModel.getBoard()
	local avatar = board:getAvatar()
	avatar:updateCoolDowns(deltaTime)
	board:updateBolts(deltaTime)
end


return module