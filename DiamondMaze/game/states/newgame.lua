local gameModel = require("game.model")
local utilitiesUrls = require("utilities.urls")
local gameMessageIds = require("game.messageids")
local gameMessages = require("game.messages")
local gameStates = require("game.states")
local module = {}

function module.start()
	gameModel.reset()
	msg.post(utilitiesUrls.URL_ORCHESTRATOR(), gameMessageIds.MESSAGEID_CHANGESTATE(), gameMessages.makeChangeState(gameStates.STATE_INPLAY()))
end

function module.finish()
end

function module.handleCommand(command)
end

return module