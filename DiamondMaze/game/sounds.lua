local SOUND_PTOO = "/sounds#ptoo"
local SOUND_TING = "/sounds#ting"
local SOUND_KEYPICKUP = "/sounds#keypickup"
local SOUND_KACLICK = "/sounds#kaclick"

local lib = {}

function lib.SOUND_PTOO() return SOUND_PTOO end
function lib.SOUND_TING() return SOUND_TING end
function lib.SOUND_KEYPICKUP() return SOUND_KEYPICKUP end
function lib.SOUND_KACLICK() return SOUND_KACLICK end

return lib