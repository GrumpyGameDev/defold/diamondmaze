local gameCommonCreature = require("game.commoncreature")
local gameConstants = require("game.constants")
local lib = {}

function lib.create(column,row)
	local instance = gameCommonCreature.create(column, row, gameConstants.ENEMYSPAWNER_HEALTH(), nil, gameConstants.ENEMYSPAWNER_SPAWNCOOLDOWN())
	return instance
end

return lib