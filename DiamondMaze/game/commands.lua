local COMMAND_UP = "up"
local COMMAND_RIGHT = "right"
local COMMAND_DOWN = "down"
local COMMAND_LEFT = "left"
local COMMAND_GREEN = "green"
local COMMAND_RED = "red"

local module = {}

function module.COMMAND_UP() return COMMAND_UP end
function module.COMMAND_RIGHT() return COMMAND_RIGHT end
function module.COMMAND_DOWN() return COMMAND_DOWN end
function module.COMMAND_LEFT() return COMMAND_LEFT end
function module.COMMAND_GREEN() return COMMAND_GREEN end
function module.COMMAND_RED() return COMMAND_RED end

return module