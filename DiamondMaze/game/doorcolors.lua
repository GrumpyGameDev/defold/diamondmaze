local gameTerrains = require("game.terrains")
local gameItems = require("game.items")
local DOORCOLOR_RED = 1
local DOORCOLOR_GREEN = 2
local DOORCOLOR_BLUE = 3
local DOORCOLOR_YELLOW = 4

local doorTerrain = {}
doorTerrain[DOORCOLOR_RED]=gameTerrains.TERRAIN_REDDOOR()
doorTerrain[DOORCOLOR_GREEN]=gameTerrains.TERRAIN_GREENDOOR()
doorTerrain[DOORCOLOR_BLUE]=gameTerrains.TERRAIN_BLUEDOOR()
doorTerrain[DOORCOLOR_YELLOW]=gameTerrains.TERRAIN_YELLOWDOOR()
local lockTerrain = {}
lockTerrain[DOORCOLOR_RED]=gameTerrains.TERRAIN_REDLOCK()
lockTerrain[DOORCOLOR_GREEN]=gameTerrains.TERRAIN_GREENLOCK()
lockTerrain[DOORCOLOR_BLUE]=gameTerrains.TERRAIN_BLUELOCK()
lockTerrain[DOORCOLOR_YELLOW]=gameTerrains.TERRAIN_YELLOWLOCK()
local keyItem = {}
keyItem[DOORCOLOR_RED]=gameItems.ITEM_REDKEY()
keyItem[DOORCOLOR_GREEN]=gameItems.ITEM_GREENKEY()
keyItem[DOORCOLOR_BLUE]=gameItems.ITEM_BLUEKEY()
keyItem[DOORCOLOR_YELLOW]=gameItems.ITEM_YELLOWKEY()

local lib = {}

function lib.DOORCOLOR_RED() return DOORCOLOR_RED end
function lib.DOORCOLOR_GREEN() return DOORCOLOR_GREEN end
function lib.DOORCOLOR_BLUE() return DOORCOLOR_BLUE end
function lib.DOORCOLOR_YELLOW() return DOORCOLOR_YELLOW end

function lib.getDoorTerrain(doorColor)
	return doorTerrain[doorColor]
end

function lib.getLockTerrain(doorColor)
	return lockTerrain[doorColor]
end
function lib.getKeyItem(doorColor)
	return keyItem[doorColor]
end

return lib