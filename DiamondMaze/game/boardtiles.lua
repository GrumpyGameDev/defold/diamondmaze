local TILE_NONE = 0
local TILE_AVATAR = 1
local TILE_WALL = 2
local TILE_EMPTY = 3
local TILE_INDICATOR = 4
local TILE_REDDOOR = 5
local TILE_GREENDOOR = 6
local TILE_BLUEDOOR = 7
local TILE_YELLOWDOOR = 8
local TILE_REDLOCK = 9
local TILE_GREENLOCK = 10
local TILE_BLUELOCK = 11
local TILE_YELLOWLOCK = 12
local TILE_REDKEY = 13
local TILE_GREENKEY = 14
local TILE_BLUEKEY = 15
local TILE_YELLOWKEY = 16
local TILE_DIAMOND = 17
local TILE_GOODIEHUT = 18
local TILE_HEALTH = 19
local TILE_MANA = 20
local TILE_EMPTYHEALTH = 21
local TILE_EMPTYMANA = 22
local TILE_AVATARBOLTNORTH = 23
local TILE_AVATARBOLTEAST = 24
local TILE_AVATARBOLTSOUTH = 25
local TILE_AVATARBOLTWEST = 26
local TILE_ENEMY = 27
local TILE_ENEMYSPAWNER = 28
local TILE_ENEMYBOLTNORTH = 29
local TILE_ENEMYBOLTEAST = 30
local TILE_ENEMYBOLTSOUTH = 31
local TILE_ENEMYBOLTWEST = 32

local lib = {}

function lib.TILE_NONE() return TILE_NONE end
function lib.TILE_AVATAR() return TILE_AVATAR end
function lib.TILE_WALL() return TILE_WALL end
function lib.TILE_EMPTY() return TILE_EMPTY end
function lib.TILE_INDICATOR() return TILE_INDICATOR end
function lib.TILE_REDDOOR() return TILE_REDDOOR end
function lib.TILE_GREENDOOR() return TILE_GREENDOOR end
function lib.TILE_BLUEDOOR() return TILE_BLUEDOOR end
function lib.TILE_YELLOWDOOR() return TILE_YELLOWDOOR end
function lib.TILE_REDLOCK() return TILE_REDLOCK end
function lib.TILE_GREENLOCK() return TILE_GREENLOCK end
function lib.TILE_BLUELOCK() return TILE_BLUELOCK end
function lib.TILE_YELLOWLOCK() return TILE_YELLOWLOCK end
function lib.TILE_REDKEY() return TILE_REDKEY end
function lib.TILE_GREENKEY() return TILE_GREENKEY end
function lib.TILE_BLUEKEY() return TILE_BLUEKEY end
function lib.TILE_YELLOWKEY() return TILE_YELLOWKEY end
function lib.TILE_DIAMOND() return TILE_DIAMOND end
function lib.TILE_GOODIEHUT() return TILE_GOODIEHUT end
function lib.TILE_HEALTH() return TILE_HEALTH end
function lib.TILE_MANA() return TILE_MANA end
function lib.TILE_EMPTYHEALTH() return TILE_EMPTYHEALTH end
function lib.TILE_EMPTYMANA() return TILE_EMPTYMANA end
function lib.TILE_AVATARBOLTNORTH() return TILE_AVATARBOLTNORTH end
function lib.TILE_AVATARBOLTEAST() return TILE_AVATARBOLTEAST end
function lib.TILE_AVATARBOLTSOUTH() return TILE_AVATARBOLTSOUTH end
function lib.TILE_AVATARBOLTWEST() return TILE_AVATARBOLTWEST end
function lib.TILE_ENEMY() return TILE_ENEMY end
function lib.TILE_ENEMYSPAWNER() return TILE_ENEMYSPAWNER end
function lib.TILE_ENEMYBOLTNORTH() return TILE_ENEMYBOLTNORTH end
function lib.TILE_ENEMYBOLTEAST() return TILE_ENEMYBOLTEAST end
function lib.TILE_ENEMYBOLTSOUTH() return TILE_ENEMYBOLTSOUTH end
function lib.TILE_ENEMYBOLTWEST() return TILE_ENEMYBOLTWEST end

return lib