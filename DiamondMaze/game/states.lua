local STATE_NONE = "none"
local STATE_NEWGAME = "new-game"
local STATE_INPLAY = "in-play"

local module = {}

function module.STATE_NONE() return STATE_NONE end
function module.STATE_NEWGAME() return STATE_NEWGAME end
function module.STATE_INPLAY() return STATE_INPLAY end

return module